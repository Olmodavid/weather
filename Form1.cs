﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace finalWebRequest
{
    public partial class Form1 : Form
    {
        PeticionWeb peticionWeb;
        private string cp = "44130";

        public Form1()
        {
            InitializeComponent();
            this.CambioClima();

        }


        private void placeholder(object sender, MouseEventArgs e)
        {
            codigoPostal.Text = "";
        }

        private void blur(object sender, EventArgs e)
        {
            codigoPostal.Text = "C.P.";
        }

        private void cambiarCp(object sender, KeyEventArgs e)
        {
            if(e.KeyValue == 13)
            {
                if(codigoPostal.Text != null)
                {
                    this.cp = codigoPostal.Text;
                    this.CambioClima();
                    codigoPostal.Text = "";
                }
                else
                {

                    MessageBox.Show(this, "Enter C.P.", "Fail");
                }
            }




        }


        private void CambioClima()
        {
            string url = @"http://api.openweathermap.org/data/2.5/weather?zip=" + this.cp + ",mx&units=metric&appid=9ae7bf879564c9b8045c204c9138d734";
            this.peticionWeb = new PeticionWeb(url);

            JObject response = this.peticionWeb.Run().getJsonResponse();


            if (response != null)
            {
                temperatura.Text = response["main"]["temp"] + " ° C";
                Ciudad.Text = (string)response["name"];
                try
                {
                    string iconUrl = "http://openweathermap.org/img/wn/" + (string)response["weather"][0]["icon"] + "@2x.png";
                    Console.WriteLine(iconUrl);
                    //string a stream
                    var iconUrlStream = this.peticionWeb.getStream(iconUrl);

                    Console.WriteLine(iconUrlStream);

    
                    pictureBox1.Image = Bitmap.FromStream(iconUrlStream);
                    pictureBox1.ImageLocation = iconUrl;

                    //Console.WriteLine(iconUrl);
                }
                catch(Exception error)
                {
                    Console.WriteLine(error.Message);
                }


            }
            else
            {
                MessageBox.Show(this, "Error en peticion", "Fail");

            }


        }
    }

}
