﻿namespace finalWebRequest
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.codigoPostal = new System.Windows.Forms.TextBox();
            this.temperatura = new System.Windows.Forms.Label();
            this.Ciudad = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // codigoPostal
            // 
            this.codigoPostal.Location = new System.Drawing.Point(12, 12);
            this.codigoPostal.Name = "codigoPostal";
            this.codigoPostal.Size = new System.Drawing.Size(100, 20);
            this.codigoPostal.TabIndex = 0;
            this.codigoPostal.Text = "C.P.";
            this.codigoPostal.MouseClick += new System.Windows.Forms.MouseEventHandler(this.placeholder);
            this.codigoPostal.KeyUp += new System.Windows.Forms.KeyEventHandler(this.cambiarCp);
            this.codigoPostal.Leave += new System.EventHandler(this.blur);
            // 
            // temperatura
            // 
            this.temperatura.AutoSize = true;
            this.temperatura.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.temperatura.Location = new System.Drawing.Point(170, 56);
            this.temperatura.Name = "temperatura";
            this.temperatura.Size = new System.Drawing.Size(60, 31);
            this.temperatura.TabIndex = 1;
            this.temperatura.Text = "0°C";
            // 
            // Ciudad
            // 
            this.Ciudad.AutoSize = true;
            this.Ciudad.Location = new System.Drawing.Point(173, 43);
            this.Ciudad.Name = "Ciudad";
            this.Ciudad.Size = new System.Drawing.Size(40, 13);
            this.Ciudad.TabIndex = 2;
            this.Ciudad.Text = "Ciudad";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox1.Location = new System.Drawing.Point(12, 56);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(166, 142);
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(311, 200);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.Ciudad);
            this.Controls.Add(this.temperatura);
            this.Controls.Add(this.codigoPostal);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox codigoPostal;
        private System.Windows.Forms.Label temperatura;
        private System.Windows.Forms.Label Ciudad;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}

