﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace finalWebRequest
{
    class PeticionWeb
    {
        private string url;
        private string respuesta;
        Stream stream1;
        

        public PeticionWeb(string url)
        {
            this.url = url;
          
        }
        public PeticionWeb Run()
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.AutomaticDecompression = DecompressionMethods.GZip;
            


            try
            {
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    this.respuesta = reader.ReadToEnd();
                    this.stream1 = stream;
                    
                }

            }
            catch(Exception e)
            {
                Console.WriteLine("Url consultada " + this.url);
                Console.WriteLine(e.Message);
                
            }
            return this;
        }

        public string getStringResponse()
        {
            return this.respuesta;
        }

        public JObject getJsonResponse()
        {
           // return this.respuesta == "" ? null : JObject.Parse(this.respuesta);
           if(this.respuesta == null)
            {
                return null;
            }
            else
            {
                return JObject.Parse(this.respuesta);
            }

        }
       
        public Stream getStream(string IconUrl)
        {
            var request = WebRequest.Create(IconUrl);
            using (var response = request.GetResponse())
            using (var stream = response.GetResponseStream())
            {
               // this.stream1 = stream;
                return stream;
            }




            /*
            if (this.stream1 == null)
            {
                return null;
            }
            else
            {
                return stream1;
            }
            */
        }
        
    }


}
